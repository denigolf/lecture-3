import * as config from './config';
import { getRandomInt } from '../helpers';
import data from '../data';

const activeUsers = [];
const users = [];
const rom = new Map();
const roomsTextId = new Map();

function userConnect(id, username) {
  const user = { id, username };
  activeUsers.push(user);
  return user;
}

function userDisconnect(id) {
  const index = activeUsers.findIndex((user) => user.id === id);

  if (index !== -1) {
    return activeUsers.splice(index, 1)[0];
  }
}

function geOnlinetUserByUsername(username) {
  return activeUsers.find((user) => user.username === username);
}

function userJoin(id, username, room) {
  const user = { id, username, room, status: false };
  users.push(user);
  return user;
}

function userLeave(id) {
  const index = users.findIndex((user) => user.id === id);

  if (index !== -1) {
    return users.splice(index, 1)[0];
  }
}

function getCurrentUser(id) {
  return users.find((user) => user.id === id);
}

function getRoomClientsCount(io, roomId) {
  return io.sockets.adapter.rooms[roomId].length;
}

function getRoomUsers(roomId) {
  return users.filter((user) => user.room === roomId);
}

export default (io) => {
  io.on('connection', (socket) => {
    socket.emit('UPDATE_ROOMS', Array.from(rom));
    const username = socket.handshake.query.username;

    if (geOnlinetUserByUsername(username)) {
      socket.emit('SHOW_ERROR', `User with this username already exists`);
      socket.emit('CLEAR_STORAGE');
    } else {
      userConnect(socket.id, username);
    }

    socket.on('CREATE_ROOM', ({ roomId, roomCount }) => {
      if (rom.get(roomId)) {
        socket.emit('SHOW_ERROR', `Room with id '${roomId}' already exist!`);
      } else if (!roomId || roomId.trim() === '') {
        socket.emit('SHOW_ERROR', `Room id cannot be empty string!`);
      } else {
        io.emit('RENDER_ROOM', { roomId, roomCount });

        rom.set(roomId, { roomId, roomCount });
        roomsTextId.set(roomId, getRandomInt(1, data.texts.length - 1));
      }
    });

    socket.on('JOIN_ROOM', (username, roomName) => {
      const user = userJoin(socket.id, username, roomName);
      user.room = roomName;
      socket.join(user.room);

      const roomUsers = getRoomUsers(user.room);

      if (roomUsers.length > config.MAXIMUM_USERS_FOR_ONE_ROOM) {
        socket.emit('SHOW_ERROR', 'Room is full!');

        return;
      }

      rom.set(roomName, {
        roomId: roomName,
        roomCount: getRoomUsers(user.room).length,
      });
      io.to(user.room).emit(
        'UPDATE_NAMES',
        getRoomUsers(user.room),
        getCurrentUser(socket.id)
      );
      io.emit('update', roomName, getRoomClientsCount(io, roomName));
    });

    socket.on('YOU', () => {
      socket.emit('UPDATE_YOU', getCurrentUser(socket.id));
    });

    socket.on('LEAVE_ROOM', () => {
      const user = userLeave(socket.id);

      if (user) {
        rom.set(user.room, {
          roomId: user.room,
          roomCount: getRoomUsers(user.room).length,
        });

        if (rom.get(user.room).roomCount === 0) {
          rom.delete(user.room);
          io.emit('UPDATE_ROOMS', Array.from(rom));
        }

        io.to(user.room).emit('UPDATE_NAMES', getRoomUsers(user.room));
        io.emit('update', user.room, getRoomUsers(user.room).length);
      }
    });

    socket.on('USER_STATUS', () => {
      const user = getCurrentUser(socket.id);

      if (user.status === false) {
        user.status = true;
      } else {
        user.status = false;
      }

      io.to(user.room).emit('UPDATE_STATUS', user);

      const roomUsers = getRoomUsers(user.room);
      if (roomUsers.every((user) => user.status)) {
        io.to(user.room).emit(
          'START_TIMER',
          config.SECONDS_TIMER_BEFORE_START_GAME
        );
      }
    });

    socket.on('INPUT_CHANGED', (user) => {
      io.emit('UPDATE_INPUT', user);
    });

    socket.on('START_GAME', () => {
      socket.emit(
        'SHOW_TEXT',
        roomsTextId.get(getCurrentUser(socket.id).room),
        getCurrentUser(socket.id),
        config.SECONDS_FOR_GAME
      );
    });

    socket.on('disconnect', () => {
      userDisconnect(socket.id);
      const user = userLeave(socket.id);
      if (user) {
        rom.set(user.room, {
          roomId: user.room,
          roomCount: getRoomUsers(user.room).length,
        });
        io.to(user.room).emit('UPDATE_NAMES', getRoomUsers(user.room));
        io.emit('update', user.room, getRoomUsers(user.room).length);
      }
    });
  });
};
