const username = sessionStorage.getItem('username');

if (!username) {
  window.location.replace('/login');
}

const socket = io('', { query: { username } });

const addRoomBtn = document.getElementById('add-room-btn');
const roomsElement = document.getElementById('rooms');
const roomsPageElement = document.getElementById('rooms-page');
const gamePageElement = document.getElementById('game-page');
const quitRoomBtn = document.getElementById('quit-room-btn');
const readyBtn = document.getElementById('ready-btn');
const timerElement = document.getElementById('timer');
const textContainerElement = document.getElementById('text-container');
const secondsForGameElement = document.getElementById('seconds-for-game');
const textInputElement = document.getElementById('text-input');

function updateRoomClientsNumber(roomName, count) {
  const room = document.getElementById(roomName);
  if (room) {
    const span = room.children[0];
    span.textContent = count + ' ' + 'users connected';
  }
}

function renderRoom({ roomId, roomCount }) {
  const room = document.createElement('div');
  room.setAttribute('id', roomId);
  room.classList.add('room');
  const roomSpan = document.createElement('span');
  roomSpan.textContent = roomCount + ' ' + 'users connected';
  const roomHeading = document.createElement('h2');
  roomHeading.textContent = roomId;
  const roomButton = document.createElement('button');
  roomButton.classList.add('join-btn');
  roomButton.textContent = 'Join Room';
  roomButton.addEventListener('click', () => {
    socket.emit('JOIN_ROOM', username, roomId);
    roomsPageElement.classList.add('display-none');
    document.querySelector('.game-info').children[0].innerText = roomId;
    gamePageElement.classList.remove('display-none');
    quitRoomBtn.addEventListener('click', () => quitRoomBtnHandler(roomId));
  });
  room.append(roomSpan);
  room.append(roomHeading);
  room.append(roomButton);
  roomsElement.append(room);
}

function createRoomButtonHandler() {
  const roomId = prompt('Enter the room name:', '');
  socket.emit('CREATE_ROOM', { roomId, roomCount: 0 });
}

function quitRoomBtnHandler() {
  roomsPageElement.classList.remove('display-none');
  gamePageElement.classList.add('display-none');
  readyBtn.innerText = 'READY';
  socket.emit('LEAVE_ROOM');
}

function countDown(seconds, element) {
  let timeLeft = seconds;
  const interval = setInterval(() => {
    if (timeLeft <= 0) {
      clearInterval(interval);
      if (element === timerElement) {
        socket.emit('START_GAME');
      } else if (element === secondsForGameElement) {
        socket.emit('FINISH');
      }
    }
    element.innerText = timeLeft--;
  }, 1000);
}

function getTimerTime() {
  return Math.floor((new Date() - startTime) / 1000);
}

let startTime;
function startTimer() {
  timerElement.innerText = 0;
  startTime = new Date();
  setInterval(() => {
    timer.innerText = getTimerTime();
  }, 1000);
}

function readyBtnHandler() {
  if (readyBtn.innerText === 'NOT READY') {
    readyBtn.innerText = 'READY';
  } else {
    readyBtn.innerText = 'NOT READY';
  }
  socket.emit('USER_STATUS', username);
}

function getTextById(id) {
  return fetch(`http://localhost:3002/game/texts/${id}`)
    .then((body) => body.text())
    .then((data) => data);
}

function game(text) {
  textInputElement.value = '';
  textContainerElement.innerHTML = '';
  text.split('').forEach((char) => {
    const span = document.createElement('span');
    span.innerText = char;
    textContainerElement.appendChild(span);
  });
}

socket.on('RENDER_ROOM', (room) => {
  renderRoom(room);
});

socket.on('UPDATE_ROOMS', (rooms) => {
  roomsElement.innerHTML = '';
  rooms.forEach((room) => {
    renderRoom(room[1]);
  });
});

socket.on('update', updateRoomClientsNumber);

socket.on('UPDATE_NAMES', (users) => {
  const usersElement = document.querySelector('.users');

  usersElement.innerHTML = '';
  users.forEach((user) => {
    const userElement = document.createElement('div');
    userElement.classList.add('user', `user-${user.id}`);
    const span = document.createElement('span');
    span.textContent = user.username;
    const status = document.createElement('div');
    if (user.status) {
      status.classList.add('status', 'ready-status-green');
    } else {
      status.classList.add('status', 'ready-status-red');
    }
    const progressBar = document.createElement('div');
    progressBar.classList.add('progress');
    const progress = document.createElement('div');
    progress.classList.add('user-progress', user.username);
    progressBar.append(progress);

    userElement.append(status);
    userElement.append(span);
    userElement.append(progressBar);
    usersElement.append(userElement);
  });

  socket.emit('YOU');
});

socket.on('UPDATE_YOU', (user) => {
  if (user) {
    const userElement = document.querySelector(`.user-${user.id}`);
    const span = userElement.querySelector('span');
    span.innerHTML = user.username + '&nbsp;&nbsp;&nbsp;👈';
  }
});

socket.on('START_TIMER', (secondsBeforeStart) => {
  quitRoomBtn.classList.add('display-none');
  readyBtn.classList.add('display-none');
  timerElement.classList.remove('display-none');
  countDown(secondsBeforeStart, timerElement);
});

socket.on('SHOW_TEXT', async (textId, user, secondsForGame) => {
  timerElement.classList.add('display-none');
  secondsForGameElement.classList.remove('display-none');
  countDown(secondsForGame, secondsForGameElement);
  textContainerElement.classList.remove('display-none');
  textInputElement.classList.remove('display-none');
  const text = await getTextById(textId);
  game(text);

  function textInputHandler() {
    const textArray = textContainerElement.querySelectorAll('span');
    const valueArray = textInputElement.value.split('');
    let exact = true;
    textArray.forEach((span, index) => {
      const char = valueArray[index];

      if (char == null) {
        span.classList.remove('exact');
        span.classList.remove('inexact');
        exact = false;
      } else if (char === span.innerText) {
        span.classList.add('exact');
        span.classList.remove('inexact');
      } else {
        span.classList.add('inexact');
        span.classList.remove('exact');
        exact = false;
      }
    });

    if (exact) {
      const progressBar = document.querySelector(`.${user.username}`);
      if (progressBar) {
        progressBar.classList.add('finished');
      }
      secondsForGameElement.classList.add('display-none');
      textInputElement.classList.add('display-none');
      textInputElement.removeEventListener('input', textInputHandler);
      alert(`User ${user.username} win!`);
    }

    socket.emit('INPUT_CHANGED', user);
  }

  textInputElement.addEventListener('input', textInputHandler);
});

socket.on('UPDATE_INPUT', (user) => {
  const textLength = textContainerElement.children.length;
  let exactCount = 0;
  const spans = textContainerElement.querySelectorAll('span');
  spans.forEach((span) => {
    if (span.classList.contains('exact')) {
      exactCount++;
    }
  });

  const percent = (exactCount / textLength) * 100;
  const progressBars = document.querySelectorAll('.user-progress');
  progressBars.forEach((bar) => {
    if (bar.classList.contains(username)) {
      const progressBar = document.querySelector(`.${user.username}`);
      if (progressBar) {
        progressBar.style.width = percent + '%';
      }
    }
  });
});

socket.on('UPDATE_STATUS', (user) => {
  const userElement = document.querySelector(`.user-${user.id}`);
  if (user.status) {
    userElement.querySelector('.status').classList.add('ready-status-green');
    userElement.querySelector('.status').classList.remove('ready-status-red');
  } else {
    userElement.querySelector('.status').classList.add('ready-status-red');
    userElement.querySelector('.status').classList.remove('ready-status-green');
  }
});

socket.on('CLEAR_STORAGE', () => {
  window.location.replace('/login');
  sessionStorage.removeItem('username');
});

socket.on('SHOW_ERROR', (message) => {
  alert(message);
});

// EVENT LISTENERS
addRoomBtn.addEventListener('click', createRoomButtonHandler);
readyBtn.addEventListener('click', readyBtnHandler);
